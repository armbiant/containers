containers = etcd-ctr
all: $(containers)

%-ctr : %/Dockerfile
	$(call docker-build,$(subst -ctr,,$(@)))

BLUE=\e[34m
GREEN=\e[32m
CYAN=\e[36m
NORMAL=\e[39m

REGISTRY ?= docker.io
TAG ?= latest
REGPORT ?=
ORG ?= mergetb

QUIET=@
SWAGGER_QUIET=-q
DOCKER_QUIET=-q
ifeq ($(V),1)
	QUIET=
	SWAGGER_QUIET=
	DOCKER_QUIET=
endif

define build-slug
	@echo "$(BLUE)$1$(GREEN)\t $< $(CYAN)$@$(NORMAL)"
endef

define docker-build
	$(call build-slug,docker)
	$(QUIET) docker build \
		${BUILD_ARGS} $(DOCKER_QUIET) -f $< \
		-t $(REGISTRY)$(REGPORT)/$(ORG)/$1:$(TAG) .
	$(if ${PUSH},$(call docker-push,$1))
endef

define docker-push
	$(call build-slug,push)
	$(QUIET) docker push $(REGISTRY)$(REGPORT)/$(ORG)/$1:$(TAG)
endef
